const express = require("express");
const app = express();
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
const cronjobs = require("./cron-jobs/cron")

const userApi = require("./controller/users");
const productApi = require("./controller/products");
const orderApi = require("./controller/orders")

const db = require("./models/index");
const { order } = require("./models/index");
db.sequelize.sync();

//Base API for checking status of API 
app.get("/", function (req, res) {
    res.send({
        status: "ON",
        message: "Api service is running!!",
    });
});

app.use("/api/", userApi);
app.use("/api/", productApi);
app.use("/api/", orderApi);
app.listen(process.env.PORT || 8888, () => {
    console.log("server is up at 8888");
});