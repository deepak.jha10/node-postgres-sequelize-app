const router = require("express").Router();
const constants = require("../utils/constants");
const httpStatus = require("http-status");
const db = require("../models/index");
const Orders = db.order;
const Users = db.user;
const notification = require("../utils/emailTemplate")
const emailService = require("../utils/emailConfig")




// api for adding users
router.post("/createOrder", async (req, res) => {
    try {
        console.log(req.body)
        let newOrder = await Orders.create(req.body)

        let user = await Users.findAll({ where: { id: req.body.userId } })
        console.log(user[0])
        
        let mailObject = notification.emailContentCreation(
            user[0],
            "Order Placed",
        );
        emailService.sendEmail(mailObject);
        res.status(200).json({
            status: httpStatus.OK,
            message: constants.constants.SUCCCESS_MSG,
            data: newOrder
        });
    } catch (exception) {
        console.log(exception)
        res.status(500).send({
            status: httpStatus.INTERNAL_SERVER_ERROR,
            message: constants.constants.FAILURE_MSG,
            exception: exception,
        });
    }
});

// api for view orders

router.get("/viewOrders", async (req, res) => {
    try {
        let offset;
        let limit;
        if (req.query.pageNo && req.query.perPage) {
            req.query.perPage = parseInt(req.query.perPage);
            req.query.pageNo = parseInt(req.query.pageNo);
            offset = req.query.perPage * (req.query.pageNo - 1);
            limit = req.query.perPage;
        } else {
            offset = 0;
            limit = 3;
        }
        let data = await Orders.findAll({ offset: offset, limit: limit });
        res.status(200).json({
            status: httpStatus.OK,
            message: constants.constants.SUCCCESS_MSG,
            noOfOrders: data.length,
            result: data
        });
    } catch (exception) {
        console.log(exception)
        res.status(500).send({
            status: httpStatus.INTERNAL_SERVER_ERROR,
            message: constants.constants.FAILURE_MSG,
            exception: exception,
        });
    }
})

// update orders

router.put("/updateOrder", async (req, res) => {
    try {
        let updatedOrder = await Orders.update(req.body, { where: { id: req.body.id } })
        res.status(200).json({
            status: httpStatus.OK,
            message: constants.constants.SUCCCESS_MSG,
            data: updatedOrder
        });
    } catch (exception) {
        res.status(500).send({
            status: httpStatus.INTERNAL_SERVER_ERROR,
            message: constants.constants.FAILURE_MSG,
            exception: exception,
        });
    }
});

// delete orders

router.delete("/delOrder", async (req, res) => {
    try {
        let deleteOrder = await Orders.destroy({ where: { id: req.body.id } });
        res.status(200).json({
            status: httpStatus.OK,
            message: constants.constants.SUCCCESS_MSG,
        });
    } catch (exception) {
        console.log(exception)
        res.status(500).send({
            status: httpStatus.INTERNAL_SERVER_ERROR,
            message: constants.constants.FAILURE_MSG,
            exception: exception,
        });
    }
});




module.exports = router;