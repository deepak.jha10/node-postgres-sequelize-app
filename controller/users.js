const router = require("express").Router();
const constants = require("../utils/constants");
const httpStatus = require("http-status");
const db = require("../models/index");
const cron = require("node-cron")
const notification = require("../utils/emailTemplate")
const emailService = require("../utils/emailConfig")
const User = db.user;


// api for adding users
router.post("/register", async (req, res) => {
    try {
        let newUser = await User.create(req.body)
        let mailObject = notification.emailContentCreation(
            newUser,
            "Account Creation",
        );
        emailService.sendEmail(mailObject);
        res.status(200).json({
            status: httpStatus.OK,
            message: constants.constants.SUCCCESS_MSG,
            data: newUser
        });
    } catch (exception) {
        console.log(exception)
        res.status(500).send({
            status: httpStatus.INTERNAL_SERVER_ERROR,
            message: constants.constants.FAILURE_MSG,
            exception: exception,
        });
    }
});

// api for view user

router.get("/viewUser", async (req, res) => {
    try {
        let offset;
        let limit;
        if (req.query.pageNo && req.query.perPage) {
            req.query.perPage = parseInt(req.query.perPage);
            req.query.pageNo = parseInt(req.query.pageNo);
            offset = req.query.perPage * (req.query.pageNo - 1);
            limit = req.query.perPage;
        } else {
            offset = 0;
            limit = 3;
        }
        let data = await User.findAll({ offset: offset, limit: limit });
        res.status(200).json({
            status: httpStatus.OK,
            message: constants.constants.SUCCCESS_MSG,
            noOfUsers: data.length,
            result: data
        });
    } catch (exception) {
        console.log(exception)
        res.status(500).send({
            status: httpStatus.INTERNAL_SERVER_ERROR,
            message: constants.constants.FAILURE_MSG,
            exception: exception,
        });
    }
})



// update user

router.put("/updateUser", async (req, res) => {
    try {
        let updatedUser = await User.update(req.body, { where: { id: req.body.id } })
        let mailObject = notification.emailContentCreation(
            newUser,
            "Update Information",
        );
        emailService.sendEmail(mailObject);
        res.status(200).json({
            status: httpStatus.OK,
            message: constants.constants.SUCCCESS_MSG,
            data: updatedUser
        });
    } catch (exception) {
        res.status(500).send({
            status: httpStatus.INTERNAL_SERVER_ERROR,
            message: constants.constants.FAILURE_MSG,
            exception: exception,
        });
    }
});

// delete user

router.delete("/delUser", async (req, res) => {
    try {
        let deleteUser = await User.destroy({ where: { id: req.body.id } });
        res.status(200).json({
            status: httpStatus.OK,
            message: constants.constants.SUCCCESS_MSG,
        });
    } catch (exception) {
        console.log(exception)
        res.status(500).send({
            status: httpStatus.INTERNAL_SERVER_ERROR,
            message: constants.constants.FAILURE_MSG,
            exception: exception,
        });
    }
});

// 

module.exports = router;