const router = require("express").Router();
const constants = require("../utils/constants");
const httpStatus = require("http-status");
const db = require("../models/index");
const Products = db.product;
const Sequelize = require("sequelize");
const Op = Sequelize.Op;


router.post("/addProducts", async (req, res) => {
    try {
        let newUser = await Products.create(req.body)
        res.status(200).json({
            status: httpStatus.OK,
            message: constants.constants.SUCCCESS_MSG,
            data: newUser
        });
    } catch (exception) {
        console.log(exception)
        res.status(500).send({
            status: httpStatus.INTERNAL_SERVER_ERROR,
            message: constants.constants.FAILURE_MSG,
            exception: exception,
        });
    }
});


// api for view user

router.get("/viewProducts", async (req, res) => {
    try {
        let offset;
        let limit;
        if (req.query.pageNo && req.query.perPage) {
            req.query.perPage = parseInt(req.query.perPage);
            req.query.pageNo = parseInt(req.query.pageNo);
            offset = req.query.perPage * (req.query.pageNo - 1);
            limit = req.query.perPage;
        } else {
            offset = 0;
            limit = 20;
        }
        let data = await Products.findAll({ offset: offset, limit: limit });
        res.status(200).json({
            status: httpStatus.OK,
            message: constants.constants.SUCCCESS_MSG,
            noOfUsers: data.length,
            result: data
        });
    } catch (exception) {
        console.log(exception)
        res.status(500).send({
            status: httpStatus.INTERNAL_SERVER_ERROR,
            message: constants.constants.FAILURE_MSG,
            exception: exception,
        });
    }
})


// api for product search

router.get("/productsSearch", async (req, res) => {
    try {
        let offset;
        let limit;
        if (req.query.pageNo && req.query.perPage) {
            req.query.perPage = parseInt(req.query.perPage);
            req.query.pageNo = parseInt(req.query.pageNo);
            offset = req.query.perPage * (req.query.pageNo - 1);
            limit = req.query.perPage;
        } else {
            offset = 0;
            limit = 20;
        }
        let keyword = req.query.KeyWord;
        let data = await Products.findAll({
            where: {
                [Op.or]: [
                    { productName: { [Op.like]: `%${keyword}%` } },
                    { productPrice: { [Op.like]: `%${keyword}%` } },
                    { desc: { [Op.like]: `%${keyword}%` } }
                ]
            },
            offset: offset,
            limit: limit
        });
        res.status(200).json({
            status: httpStatus.OK,
            message: constants.constants.SUCCCESS_MSG,
            noOfProducts: data.length,
            result: data
        });
    } catch (exception) {
        console.log(exception)
        res.status(500).send({
            status: httpStatus.INTERNAL_SERVER_ERROR,
            message: constants.constants.FAILURE_MSG,
            exception: exception,
        });
    }
})



module.exports = router;