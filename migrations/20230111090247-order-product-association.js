'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.addConstraint("Orders", {
      fields: ["productId"],
      type: "foreign key",
      name: "order_product_associations",
      references: {
        table: "Products",
        field: "id"
      }
    })
  },

  async down(queryInterface, Sequelize) {
    queryInterface.removeConstraint("Orders", "order_product_associations")

    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
