const cron = require("node-cron")
const router = require("express").Router();
const constants = require("../utils/constants");
const httpStatus = require("http-status");
const db = require("../models/index");
const notification = require("../utils/emailTemplate")
const emailService = require("../utils/emailConfig")
const User = db.user;




cron.schedule("*/10 * * * * *", function () {
    mailService();
});

async function mailService() {
    let user = await User.findAll({})
    for (let i = 0; i < user.length; i++) {
        let mailObject = notification.emailContentCreation(
            user[i],
            "Daily Offer",
        );
        emailService.sendEmail(mailObject);
    }
    console.log("cron is running")
}













module.exports = router;