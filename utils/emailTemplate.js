// mail templates for user services

let emailContentCreation = (user, notification) => {
    let finalObject = {};
    switch (notification) {
        case "Account Creation":
            finalObject.subject = `Account creation confirmation mail.`;
            finalObject.text = `Hi ${user.firstName + " " + user.lastName}, you have been register to our website. Please visit us for new collection`
            finalObject.emailTo = user.email;
            break;
        case "Update Information":
            finalObject.subject = `Email for update info for user.`;
            finalObject.text = `Hi ${user.firstName + " " + user.lastName}, you have updated your profile.`
            finalObject.emailTo = user.email;
            break;
        case "Order Placed":
            finalObject.subject = `Confimation for placing order.`;
            finalObject.text = `Hi ${user.firstName + " " + user.lastName}, your order has been placed. Checkout us for more products`
            finalObject.emailTo = user.email;
            break;
        case "Daily Offer":
            finalObject.subject = `Special Offer Only For You`;
            finalObject.text = `Hi ${user.firstName + " " + user.lastName}, you have special offer on new collections.Please visit us for the more information`
            finalObject.emailTo = user.email;
            break;
    }
    return finalObject;
};

module.exports = {
    emailContentCreation,
};